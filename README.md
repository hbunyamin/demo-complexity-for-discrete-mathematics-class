# The 8 time complexities a developer should know

This repo is inspired by a [blog](https://adrianmejia.com/most-popular-algorithms-time-complexity-every-programmer-should-know-free-online-tutorial-course/) by Adrian Mejia.     
Thank you, Adrian!

Specifically, this notebook is used to teach students in Discrete Mathematics class at [Maranatha Christian University](http://www.maranatha.edu).     
     
Feel free to utilize this notebook. Moreover, if you have any questions, also feel free to send (an) email(s) to hendra.bunyamin@it.maranatha.edu.    
     
Enjoy!
