plotExample3 <- function(){
  x <- seq(0, 10, 0.01);
  y <- 2*x^2 + 3*x +4;
  g1 <- x^2 + x;
  g2 <- 3*x^2 + 3*x; 
  plot(x, y, type = "l", col="blue");
  lines(x, g1, col="red");
  lines(x, g2, col="orange");
  legend("topleft", legend=c("3x^2+3x", "2x^2+3x+4", "x^2+x"), col=c("orange", "blue", "red"), lty=c(1,1,1));
}